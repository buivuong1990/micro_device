exports.up = function(knex, Promise) {
    return knex.schema.createTable('imeis', function(t){
        t.increments('id').primary();
        t.string('imei');
        t.integer('model_id');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('imeis');
};
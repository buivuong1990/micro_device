var express = require("express");
var knex = require("../config/knex");
var multer  = require('multer')
var router = express.Router();
var path = require('path');
var fs = require('fs');

router.post('/imei/check', function(req, res){
    var imei = req.body.imei;
    var query = knex('imeis')
                .join('models', 'models.id', '=', 'imeis.model_id')
                .join('categories', 'categories.id', '=', 'models.category_id')
                .join('brands', 'brands.id', '=', 'models.brand_id')
                .join('colors', 'colors.id', '=', 'models.color_id')
                .join('capacities', 'capacities.id', '=', 'models.capacity_id')
                .join('rams', 'rams.id', '=', 'models.ram_id')
                .select({
                    id: 'imeis.id',
                    imei: 'imeis.imei',
                    model_id: 'models.id',
                    model_name: 'models.name',
                    category_id: 'categories.id',
                    category_name: 'categories.name',
                    brand_id: 'brands.id',
                    brand_name: 'brands.name',
                    color_id: 'colors.id',
                    color_name: 'colors.name',
                    capacity_id: 'capacities.id',
                    capacity_name: 'capacities.name',
                    ram_id: 'rams.id',
                    ram_name: 'rams.name'
                })
                .from('imeis')
                .where({
                    imei: imei
                });
    query
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/list/image', function(req, res){
    var device_id = req.body.device_id;
    var query = knex('device_images')
                .select('url')
                .where('device_id', device_id)
                .orderBy('device_images.checked','1')
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/list', function(req, res){
    var email = req.body.email;
    var offset = req.body.offset;
    var limit = req.body.limit;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .join('imeis', 'imeis.imei', '=', 'devices.imei')
            .join('models', 'models.id', '=', 'imeis.model_id')
            .join('categories', 'categories.id', '=', 'models.category_id')
            .join('brands', 'brands.id', '=', 'models.brand_id')
            .join('colors', 'colors.id', '=', 'models.color_id')
            .join('capacities', 'capacities.id', '=', 'models.capacity_id')
            .join('rams', 'rams.id', '=', 'models.ram_id')
            .leftOuterJoin('device_images', function () {
                this
                    .on('device_images.device_id', 'devices.id')
                    .on('device_images.checked', 1);
            })
            .leftOuterJoin('device_availables', function(){
                this
                    .on('device_availables.device_id', 'devices.id')
            })
            .select({
                id: 'devices.id',
                available_id: 'device_availables.id',
                imei_id: 'imeis.id',
                imei: 'imeis.imei',
                price: 'devices.price',
                condition: 'devices.condition',
                created_at: 'devices.created_at',
                model_id: 'models.id',
                model_name: 'models.name',
                category_id: 'categories.id',
                category_name: 'categories.name',
                brand_id: 'brands.id',
                brand_name: 'brands.name',
                color_id: 'colors.id',
                color_name: 'colors.name',
                capacity_id: 'capacities.id',
                capacity_name: 'capacities.name',
                ram_id: 'rams.id',
                ram_name: 'rams.name',
                thumb: 'device_images.url'
            })
            .from('devices')
            .whereRaw('devices.user_id = '+user_id)
            .orderBy('devices.created_at', 'desc')
            .limit(limit)
            .offset(offset)
            .then(function(list){
                return knex
                .join('imeis', 'imeis.imei', '=', 'devices.imei')
                .join('models', 'models.id', '=', 'imeis.model_id')
                .join('categories', 'categories.id', '=', 'models.category_id')
                .join('brands', 'brands.id', '=', 'models.brand_id')
                .join('colors', 'colors.id', '=', 'models.color_id')
                .join('capacities', 'capacities.id', '=', 'models.capacity_id')
                .join('rams', 'rams.id', '=', 'models.ram_id')
                .leftOuterJoin('device_images', function () {
                    this
                        .on('device_images.device_id', 'devices.id')
                        .on('device_images.checked', 1);
                })
                .leftOuterJoin('device_availables', function(){
                    this
                        .on('device_availables.device_id', 'devices.id')
                })
                .select({
                    id: 'devices.id',
                    available_id: 'device_availables.id',
                    imei_id: 'imeis.id',
                    imei: 'imeis.imei',
                    price: 'devices.price',
                    condition: 'devices.condition',
                    created_at: 'devices.created_at',
                    model_id: 'models.id',
                    model_name: 'models.name',
                    category_id: 'categories.id',
                    category_name: 'categories.name',
                    brand_id: 'brands.id',
                    brand_name: 'brands.name',
                    color_id: 'colors.id',
                    color_name: 'colors.name',
                    capacity_id: 'capacities.id',
                    capacity_name: 'capacities.name',
                    ram_id: 'rams.id',
                    ram_name: 'rams.name',
                    thumb: 'device_images.url'
                })
                .from('devices')
                .whereRaw('devices.user_id = '+user_id)
                .orderBy('devices.created_at', 'desc')
                .then(function(listAll){
                    res.json({status: 200, data: {list: list, total: listAll.length}});
                })
                .catch(function(error){
                    res.status(500).json();
                })
            })
            .catch(function(error){
                res.status(500).json();
            })
        }else{
            res.json({status: 500});
        }
    })
});
router.get('/device/:id', function(req, res){
    var id = req.params.id;
    var query = knex
            .join('imeis', 'imeis.imei', '=', 'devices.imei')
            .join('models', 'models.id', '=', 'imeis.model_id')
            .join('categories', 'categories.id', '=', 'models.category_id')
            .join('brands', 'brands.id', '=', 'models.brand_id')
            .join('colors', 'colors.id', '=', 'models.color_id')
            .join('capacities', 'capacities.id', '=', 'models.capacity_id')
            .join('rams', 'rams.id', '=', 'models.ram_id')
            .leftOuterJoin('device_images', function () {
                this
                    .on('device_images.device_id', 'devices.id')
                    .on('device_images.checked', 1);
            })
            .leftOuterJoin('carts', 'carts.device_id', '=', 'devices.id')
            .select({
                id: 'devices.id',
                imei_id: 'imeis.id',
                imei: 'imeis.imei',
                price: 'devices.price',
                condition: 'devices.condition',
                created_at: 'devices.created_at',
                model_id: 'models.id',
                model_name: 'models.name',
                category_id: 'categories.id',
                category_name: 'categories.name',
                brand_id: 'brands.id',
                brand_name: 'brands.name',
                color_id: 'colors.id',
                color_name: 'colors.name',
                capacity_id: 'capacities.id',
                capacity_name: 'capacities.name',
                ram_id: 'rams.id',
                ram_name: 'rams.name',
                thumb: 'device_images.url',
                cart_id: 'carts.id'
            })
            .from('devices')
            .whereRaw('devices.id = '+id);
    query
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/add', function(req, res){
    var imei = req.body.imei;
    var price = req.body.price;
    var condition = req.body.condition;
    var email = req.body.email;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            var now = knex.fn.now();

            return knex('devices')
            .insert({
                imei: imei,
                price: price,
                condition: condition,
                user_id: user_id,
                created_at: now
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/edit', function(req, res){
    var id = req.body.id;
    var price = req.body.price;
    var condition = req.body.condition;

    knex('devices')
    .update({
        price: price,
        condition: condition
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.get('/device/image/:device_id', function(req, res){
    var device_id = req.params.device_id;
    var query = knex('device_images')
                .select('id', 'url', 'checked')
                .where('device_id', device_id)
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/image', function(req, res, next){
    var storage = multer.diskStorage({
        destination: function(req, file, cb){
            var dest = path.resolve(__dirname, '..', 'uploads', 'devices');
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    
    var upload = multer({ storage: storage });
    upload.single('photo')(req, res, function(err){
        if (err instanceof multer.MulterError) {
            res.json({status: 400});
        } else if (err) {
            res.json({status: 400});
        }
        var id = req.body.id;
        var filename = req.file.filename;

        knex('device_images').count('id as c').where({'device_id': id})
        .then(function(count){
            let checked = 0;
            if(count[0].c == 0){
                checked = 1;
            }
            return knex('device_images')
            .insert({
                url: filename,
                device_id: id,
                checked: checked
            })
        })
        .then(function(created){
            if(created.length > 0){
                return knex('device_images').select('id', 'url', 'checked').where({'id': created[0]})
            }else
                res.json({status: 500});
        })
        .then(function(rows){
            if(rows.length > 0){
                res.json({data: rows[0], status: 200});
            }else{
                res.json({status: 400});
            }
        })
        .catch(function(error){
            res.status(500).json();
        })
    })
});

router.delete('/device/image', function(req, res){
    var id = req.body.id;
    var query = knex('device_images')
                .select('id', 'url', 'checked')
                .where('id', id)
    query.then(function(rows){
        if(rows.length > 0){
            knex('device_images')
            .where('id', id)
            .del()
            .then(function(deleted){
                var file = path.resolve(__dirname, '..', 'uploads', 'devices', rows[0].url);
                fs.unlink(file, (err) => {
                    if (err) {
                        res.status(500).json();
                        return;
                    }
                    
                })
                res.json({status: 200, data: deleted});
            })
            .catch(function(error){
                res.status(500).json();
            });
        }else{
            res.json({status: 500});
        }
    })
});

router.post('/device/image/check', function(req, res){
    var prev_id = req.body.prev_id;
    var post_id = req.body.post_id;

    knex.transaction(function(trx) {
        knex('device_images')
        .transacting(trx)
        .update({
            checked: false
        })
        .where('id', prev_id)
        .then(function(updated){
            if(updated)
                return knex('device_images')
                .transacting(trx)
                .update({
                    checked: true
                })
                .where('id', post_id)
            else
                trx.rollback
        })
        .then(trx.commit)
        .catch(trx.rollback)
    })
    .then(function(updated){
        res.json({status: 200, data: updated});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/available/add', function(req, res){
    var sale_price = req.body.sale_price ? req.body.sale_price : null;
    var exchange_price = req.body.exchange_price ? req.body.exchange_price : null;
    var device_id = req.body.device_id;
    var model_id = req.body.model_id ? req.body.model_id : null;
    var type = req.body.type;

    if(type > 1)
        knex('models').select('name').where({'id': model_id})
        .then(function(rows){
            if(rows.length > 0){
                var name = rows[0].name;

                return knex('device_availables')
                .insert({
                    sale_price: sale_price,
                    exchange_price: exchange_price,
                    device_id: device_id,
                    model_id: model_id,
                    model_name: name,
                    type: type
                })
            }else{
                res.json({status: 500});
            }
        })
        .then(function(created){
            if(created.length > 0)
                res.json({status: 200, data: created[0]});
            else
                res.json({status: 500});
        })
        .catch(function(error){
            res.status(500).json();
        })
    else
        knex('device_availables')
        .insert({
            sale_price: sale_price,
            exchange_price: exchange_price,
            device_id: device_id,
            model_id: model_id,
            model_name: null,
            type: type
        })
        .then(function(created){
            if(created.length > 0)
                res.json({status: 200, data: created[0]});
            else
                res.json({status: 500});
        })
        .catch(function(error){
            res.status(500).json();
        })
});

router.post('/device/available/edit', function(req, res){
    var sale_price = req.body.sale_price ? req.body.sale_price : null;
    var exchange_price = req.body.exchange_price ? req.body.exchange_price : null;
    var device_id = req.body.device_id;
    var model_id = req.body.model_id ? req.body.model_id : null;
    var type = req.body.type;
    var id = req.body.id;

    if(type == 1){
        exchange_price = model_id = null;
    }else if(type == 2){
        sale_price = null;
    }

    if(type > 1)
        knex('models').select('name').where({'id': model_id})
        .then(function(rows){
            if(rows.length > 0){
                var name = rows[0].name;

                return knex('device_availables')
                .update({
                    sale_price: sale_price,
                    exchange_price: exchange_price,
                    device_id: device_id,
                    model_id: model_id,
                    model_name: name,
                    type: type
                })
                .where('id', id)
            }else{
                res.json({status: 500});
            }
        })
        .then(function(updated){
            if(updated)
                res.json({status: 200, data: updated});
            else
                res.json({status: 500});
        })
        .catch(function(error){
            res.status(500).json();
        })
    else
        knex('device_availables')
        .update({
            sale_price: sale_price,
            exchange_price: exchange_price,
            device_id: device_id,
            model_id: model_id,
            model_name: null,
            type: type
        })
        .where('id', id)
        .then(function(updated){
            if(updated)
                res.json({status: 200, data: updated});
            else
                res.json({status: 500});
        })
        .catch(function(error){
            res.status(500).json();
        })
});

router.get('/device/available/:id', function(req, res){
    var id = req.params.id;
    var query = knex
            .leftOuterJoin('models', 'models.id', '=', 'device_availables.model_id')
            .leftOuterJoin('categories', 'categories.id', '=', 'models.category_id')
            .leftOuterJoin('brands', 'brands.id', '=', 'models.brand_id')
            .select({
                id: 'device_availables.id',
                model_id: 'device_availables.model_id',
                category_id: 'categories.id',
                brand_id: 'brands.id',
                sale_price: 'device_availables.sale_price',
                exchange_price: 'device_availables.exchange_price',
                type: 'device_availables.type'
            })
            .from('device_availables')
            .whereRaw('device_availables.id = '+id);
    query
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.delete('/device/available', function(req, res){
    var id = req.body.id;
    knex('device_availables')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.get('/model/list/:category_id/:brand_id', function(req, res){
    var category_id = req.params.category_id;
    var brand_id = req.params.brand_id;
    knex('models')
    .select('id', 'name')
    .where({
        category_id: category_id,
        brand_id: brand_id
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/model/name/:id', function(req, res){
    var id = req.params.id;
    var query = knex('models')
                .join('categories', 'categories.id', '=', 'models.category_id')
                .join('brands', 'brands.id', '=', 'models.brand_id')
                .select({
                    id: 'models.id',
                    model_name: 'models.name',
                    category_id: 'categories.id',
                    category_name: 'categories.name',
                    brand_id: 'brands.id',
                    brand_name: 'brands.name',
                })
                .from('models')
                .where({
                    'models.id': id
                });
    query
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/brand/list', function(req, res){
    knex('brands')
    .select('id', 'name', 'image')
    .orderBy('id','desc')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/category/list', function(req, res){
    knex('categories')
    .select('id', 'name')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/color/list', function(req, res){
    knex('colors')
    .select('id', 'name', 'code')
    .orderBy('id','desc')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/capacity/list', function(req, res){
    knex('capacities')
    .select('id', 'name')
    .orderBy('id','desc')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/ram/list', function(req, res){
    knex('rams')
    .select('id', 'name')
    .orderBy('id','desc')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/list/wishlist', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name
                    FROM imeis
                    INNER JOIN models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                INNER JOIN (
                    SELECT wishlist.id, wishlist.device_id
                    FROM wishlist
                    WHERE wishlist.user_id = `+user_id+`
                ) AS wishlist ON wishlist.device_id = devices.id
                WHERE devices.user_id <> `+user_id+`
                AND availables.type > 0
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY devices.created_at DESC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/wishlist', function(req, res){
    var email = req.body.email;
    var device_id = req.body.device_id;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('wishlist')
            .insert({
                user_id: user_id,
                device_id: device_id
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.delete('/user/wishlist', function(req, res){
    var id = req.body.id;
    knex('wishlist')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/device/search/add/', function(req, res){
    var category_id = req.body.category_id;
    var brand_id = req.body.brand_id;
    var model_id = req.body.model_id;
    var capacity_id = req.body.capacity_id;
    var color_id = req.body.color_id;
    var ram_id = req.body.ram_id;

    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('search_devices')
            .insert({
                category_id: category_id,
                brand_id: brand_id,
                model_id: model_id,
                capacity_id: capacity_id,
                color_id: color_id,
                ram_id: ram_id,
                user_id: user_id
            })
        }else
            res.json({status: 400});
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/searchWishList', function(req, res){
    var email = req.body.email;
    var id =req.body.id;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
            `SELECT
                availables.sale_price as sale_price,
                availables.exchange_price as exchange_price,
                availables.exchange_name as exchange_name,
                availables.id as available_id,
                availables.type as type,
                imeiss.device_name as device_name,
                device_images.url as thumb,
                devices.id as id,
                imeiss.model_id as model_id,
                imeiss.capacity_id as capacity_id,
                imeiss.ram_id as ram_id,
                imeiss.color_id as color_id
            FROM devices,search_devices
            LEFT OUTER JOIN (
                SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                    device_availables.type, models.name as exchange_name
                FROM device_availables
                LEFT OUTER JOIN models ON device_availables.model_id = models.id
            ) AS availables ON devices.id = availables.device_id
            INNER JOIN (
                SELECT imeis.imei, models.name as device_name,imeis.model_id as model_id, models.capacity_id as capacity_id,models.ram_id as ram_id, models.color_id as color_id
                FROM imeis
                INNER JOIN models ON imeis.model_id = models.id
            ) as imeiss ON imeiss.imei = devices.imei
            LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
            INNER JOIN (
                SELECT wishlist.id, wishlist.device_id
                FROM wishlist
                WHERE wishlist.user_id = `+user_id+`
            ) AS wishlist ON wishlist.device_id = devices.id
            WHERE search_devices.id=`+id+` 
            AND search_devices.model_id = imeiss.model_id 
            AND search_devices.capacity_id = imeiss.capacity_id AND search_devices.ram_id = imeiss.ram_id AND search_devices.color_id = imeiss.color_id 
            AND availables.type > 0
            AND devices.id NOT IN(
                SELECT device_id
                FROM carts
                WHERE carts.user_id=`+user_id+`
            )
            AND devices.id NOT IN(
                SELECT device_id
                FROM transactions
                WHERE transactions.user_id=`+user_id+`
            )
            ORDER BY devices.created_at DESC
            `)
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/list/search', function(req, res){
    var email = req.body.email;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `
                SELECT models.name as model_name,
                categories.name as category_name,
                brands.name as brand_name,
                rams.name as ram_name,
                capacities.name as capacity_name,
                colors.name as color_name,
                models.id as model_id,
                categories.id as category_id,
                brands.id as brand_id,
                rams.id as ram_id,
                capacities.id as capacity_id,
                colors.id as color_id,
                sd.user_id as user_id,
                sd.id as id
                FROM search_devices sd
                INNER JOIN categories ON categories.id = sd.category_id
                INNER JOIN brands ON brands.id = sd.brand_id
                INNER JOIN models ON models.id = sd.model_id
                INNER JOIN rams ON rams.id = sd.ram_id
                INNER JOIN capacities ON capacities.id = sd.capacity_id
                INNER JOIN colors ON colors.id = sd.color_id
                WHERE sd.user_id=`+user_id+`  
                ORDER BY sd.id ASC
                `
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/device/search/delete', function(req, res){
    var id = req.body.id;
    knex('search_devices')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/device/search/exists', function(req, res){
    var email = req.body.email;
    var model_id = req.body.model_id;
    var capacity_id = req.body.capacity_id;
    var color_id = req.body.color_id;
    var ram_id = req.body.ram_id;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('search_devices').select('id').where({
                'user_id': user_id,
               
                'model_id': model_id,
                'capacity_id': capacity_id,
                'color_id': color_id,
                'ram_id': ram_id
                })
            
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/deviceMobile/list/type', function(req, res){
    var email = req.body.email;
    var type = req.body.type;
    if(Number(type) === 4) type = '1, 2';
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name
                    FROM imeis
                    INNER JOIN models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.user_id <> `+user_id+`
                AND availables.type IN(`+type+`, 3)
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY devices.created_at DESC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/device/list/type', function(req, res){
    var email = req.body.email;
    var type = req.body.type;
    var offset = req.body.offset;
    var limit = req.body.limit;
    if(Number(type) === 4) type = '1, 2';
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name
                    FROM imeis
                    INNER JOIN models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.user_id <> `+user_id+`
                AND availables.type IN(`+type+`, 3)
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY devices.created_at DESC
                LIMIT `+limit+`
                OFFSET `+offset+``
                
            )
            .then(function(list){
                knex.raw(
                    `SELECT availables.sale_price as sale_price,
                        availables.exchange_price as exchange_price,
                        availables.exchange_name as exchange_name,
                        availables.id as available_id,
                        availables.type as type,
                        imeiss.device_name as device_name,
                        device_images.url as thumb,
                        devices.id as id
                    FROM devices
                    LEFT OUTER JOIN (
                        SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                            device_availables.type, models.name as exchange_name
                        FROM device_availables
                        LEFT OUTER JOIN models ON device_availables.model_id = models.id
                    ) as availables ON devices.id = availables.device_id
                    INNER JOIN (
                        SELECT imeis.imei, models.name as device_name
                        FROM imeis
                        INNER JOIN models ON imeis.model_id = models.id
                    ) as imeiss ON imeiss.imei = devices.imei
                    LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                    WHERE devices.user_id <> `+user_id+`
                    AND availables.type IN(`+type+`, 3)
                    AND devices.id NOT IN(
                        SELECT device_id
                        FROM carts
                        WHERE carts.user_id=`+user_id+`
                    )
                    AND devices.id NOT IN(
                        SELECT device_id
                        FROM transactions
                        WHERE transactions.user_id=`+user_id+`
                    )
                    ORDER BY devices.created_at DESC`
                )
                .then(function(listAll){
                    res.json({status: 200, data: {list: list, total: listAll.length}});
                })
                .catch(function(error){
                    res.status(500).json();
                })
            })
            .catch(function(error){
                res.status(500).json();
            })
        }else{
            res.json({status: 500});
        }
    })
});

router.post('/device/list/featured', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name
                    FROM imeis
                    INNER JOIN models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.user_id <> `+user_id+`
                AND availables.type > 0
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY devices.created_at DESC
                LIMIT 10 OFFSET 0`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.get('/device/detail/:id/:email', function(req, res){
    var id = req.params.id;
    var email = req.params.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
                `
                SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    imeiss.category_name as category_name,
                    imeiss.brand_name as brand_name,
                    imeiss.color_name as color_name,
                    imeiss.capacity_name as capacity_name,
                    imeiss.ram_name as ram_name,
                    devices.condition as device_condition,
                    device_images.url as thumb,
                    devices.id as id,
                    carts.id as cart_id,
                    carts.type as cart_type,
                    wishlist.id as isWishlist
                FROM devices
                INNER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name, models.category_name, models.brand_name,
                        models.color_name, models.capacity_name, models.ram_name
                    FROM imeis
                    INNER JOIN (
                        SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                            colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                        FROM models
                        INNER JOIN categories ON models.category_id = categories.id
                        INNER JOIN brands ON models.brand_id = brands.id
                        INNER JOIN colors ON models.color_id = colors.id
                        INNER JOIN capacities ON models.capacity_id = capacities.id
                        INNER JOIN rams ON models.ram_id = rams.id
                    ) AS models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                LEFT OUTER JOIN (
                    SELECT carts.device_id, carts.id, carts.type 
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                ) AS carts ON carts.device_id = devices.id
                LEFT OUTER JOIN (
                    SELECT wishlist.id, wishlist.device_id
                    FROM wishlist
                    WHERE wishlist.device_id = `+id+` AND wishlist.user_id = `+user_id+`
                ) AS wishlist ON wishlist.device_id = devices.id
                WHERE devices.id=`+id+`
                `
            )
        }
    })
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.get('/device/related/:id/:email', function(req, res){
    var id = req.params.id;
    var email = req.params.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
                `
                SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name
                    FROM imeis
                    INNER JOIN models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.id <> `+id+`
                AND devices.user_id <> `+user_id+`
                AND availables.type > 0
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY devices.created_at DESC
                LIMIT 10 OFFSET 0
                `
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});


router.post('/proposal/add', function(req, res){
    var cart_id = req.body.cart_id;
    var type = req.body.type;
    var sale_proposal_price = null;
    var exchange_proposal_price = null;
    var exchange_proposal_device = null;
    var status = 'created';
    var email = req.body.email;
    var created_at = knex.fn.now();

    if(type === 1){
        sale_proposal_price = req.body.sale_proposal_price;
    }else if(type === 2){
        exchange_proposal_price = req.body.exchange_proposal_price;
        exchange_proposal_device = req.body.exchange_proposal_device;
    }

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('proposals')
            .insert({
                cart_id: cart_id,
                user_id: user_id,
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status,
                created_at: created_at
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/proposal/edit', function(req, res){
    var cart_id = req.body.cart_id;
    var type = req.body.type;
    var sale_proposal_price = null;
    var exchange_proposal_price = null;
    var exchange_proposal_device = null;
    var status = req.body.status ? req.body.status : 'updated';
    var email = req.body.email;
    var id = req.body.id;

    if(type === 1){
        sale_proposal_price = req.body.sale_proposal_price;

    }else if(type === 2){
        exchange_proposal_price = req.body.exchange_proposal_price;
        exchange_proposal_device = req.body.exchange_proposal_device;
    }

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('proposals')
            .update({
                cart_id: cart_id,
                user_id: user_id,
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status
            })
            .where('id', id)
        }else{
            return knex('proposals')
            .update({
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status
            })
            .where('id', id)
        }
    })
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.delete('/proposal/cancel', function(req, res){
    var id = req.body.id;
    knex('proposals')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});

router.post('/proposal/received/list', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT proposals.id AS id,
                    proposals.sale_proposal_price,
                    proposals.exchange_proposal_price,
                    proposals.status as proposal_status,
                    proposals.type as proposal_type,
                    carts.device_name as device_name,
                    carts.brand_name as brand_name,
                    carts.color_name as color_name,
                    carts.capacity_name as capacity_name,
                    carts.thumb
                FROM proposals
                INNER JOIN (
                    SELECT carts.id, devices.device_name AS device_name,
                        devices.brand_name AS brand_name,
                        devices.color_name AS color_name,
                        devices.capacity_name AS capacity_name,
                        devices.thumb
                    FROM carts
                    INNER JOIN (
                        SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name, imeiss.capacity_name,
                            device_images.url as thumb
                        FROM devices
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.brand_name,
                                models.color_name, models.capacity_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) AS imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                        WHERE devices.user_id = `+user_id+`
                    ) AS devices ON carts.device_id = devices.id
                ) AS carts ON proposals.cart_id = carts.id
                ORDER BY proposals.created_at DESC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.get('/proposal/received/detail/:id/:email', function(req, res){
    var id = req.params.id;
    var email = req.params.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
                `
                SELECT proposals.id AS id,
                    proposals.sale_proposal_price,
                    proposals.exchange_proposal_price,
                    proposals.status as proposal_status,
                    proposals.type as proposal_type,
                    proposals.user_id as user_id,
                    proposals.recommend_device,
                    users.email as received_email,
                    carts.device_name as device_name,
                    carts.brand_name as brand_name,
                    carts.color_name as color_name,
                    carts.capacity_name as capacity_name,
                    carts.thumb,
                    carts.id as cart_id,
                    devices.id as exchange_device_id,
                    devices.device_name as exchange_device_name,
                    devices.thumb as exchange_device_thumb,
                    devices.brand_name as exchange_device_brand_name,
                    devices.color_name as exchange_device_color_name,
                    devices.capacity_name as exchange_device_capacity_name
                FROM proposals
                INNER JOIN (
                    SELECT carts.id, devices.device_name AS device_name,
                        devices.brand_name AS brand_name,
                        devices.color_name AS color_name,
                        devices.capacity_name AS capacity_name,
                        devices.thumb
                    FROM carts
                    INNER JOIN (
                        SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name, imeiss.capacity_name,
                            device_images.url as thumb
                        FROM devices
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.brand_name,
                                models.color_name, models.capacity_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) AS imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                        WHERE devices.user_id = `+user_id+`
                    ) AS devices ON carts.device_id = devices.id
                ) AS carts ON proposals.cart_id = carts.id
                LEFT OUTER JOIN (
                    SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name,
                        imeiss.capacity_name, device_images.url as thumb
                    FROM devices
                    INNER JOIN (
                        SELECT imeis.imei, models.name as device_name, models.brand_name,
                            models.color_name, models.capacity_name
                        FROM imeis
                        INNER JOIN (
                            SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                            FROM models
                            INNER JOIN categories ON models.category_id = categories.id
                            INNER JOIN brands ON models.brand_id = brands.id
                            INNER JOIN colors ON models.color_id = colors.id
                            INNER JOIN capacities ON models.capacity_id = capacities.id
                            INNER JOIN rams ON models.ram_id = rams.id
                        ) AS models ON imeis.model_id = models.id
                    ) as imeiss ON imeiss.imei = devices.imei
                    LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                ) as devices ON devices.id = proposals.exchange_proposal_device
                INNER JOIN users ON users.id = proposals.user_id
                WHERE proposals.id = `+id
            )
        }
    })
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/category', function(req, res){
    var email = req.body.email;
    var filter = req.body.filter;
    var offset = req.body.offset;
    var limit = req.body.limit;
    var whereType = "";

    if(filter.type.length > 0){
        whereType = "AND availables.type IN ("+filter.type.toString()+",3)";
    }

    var whereCategory = "";
    if(filter.category.length > 0){
        whereCategory = "WHERE categories.id IN ("+filter.category.toString()+")";
    }

    var whereBrand = "";
    if(filter.brand.length > 0)
        whereBrand = "WHERE brands.id IN ("+filter.brand.toString()+")";

    var whereCapacity = "";
    if(filter.capacity.length > 0)
        whereCapacity = "WHERE capacities.id IN ("+filter.capacity.toString()+")";

    var whereColor = "";
    if(filter.color.length > 0)
        whereColor = "WHERE colors.id IN ("+filter.color.toString()+")";

    var whereRam = "";
    if(filter.ram.length > 0)
        whereRam = "WHERE rams.id IN ("+filter.ram.toString()+")";

    var orderBy = "devices.created_at DESC";
    if(filter.orderBy > 1){
        if(filter.orderBy == 2)
            orderBy = "imeiss.device_name ASC";
        else{
            orderBy = "imeiss.device_name DESC";
        }
    }

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            knex
            .raw(
                `
                SELECT 
                    imeiss.category_name as category_name,
                    imeiss.brand_name as brand_name,
                    imeiss.ram_name as ram_name,
                    imeiss.color_name as color_name,
                    availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id
                FROM devices
                INNER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name, models.brand_name, models.category_name,
                        models.ram_name, models.capacity_name, models.color_name
                    FROM imeis
                    INNER JOIN (
                        SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                            colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                        FROM models
                        INNER JOIN (
                            SELECT categories.id, categories.name
                            FROM categories
                            `+whereCategory+`
                        )AS categories ON models.category_id = categories.id
                        INNER JOIN (
                            SELECT brands.id, brands.name
                            FROM brands
                            `+whereBrand+`
                        ) AS brands ON models.brand_id = brands.id
                        INNER JOIN (
                            SELECT colors.id, colors.name
                            FROM colors
                            `+whereColor+`
                        ) AS colors ON models.color_id = colors.id
                        INNER JOIN (
                            SELECT capacities.id, capacities.name
                            FROM capacities
                            `+whereCapacity+`
                        ) AS capacities ON models.capacity_id = capacities.id
                        INNER JOIN (
                            SELECT rams.id, rams.name
                            FROM rams
                            `+whereRam+`
                        )rams ON models.ram_id = rams.id
                    ) AS models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.user_id <> `+user_id+`
                `+whereType+`
                AND availables.type > 0
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY `+orderBy+`
                LIMIT `+limit+`
                OFFSET `+offset+`
                `
            )
            .then(function(list){
                knex
                .raw(
                    `
                    SELECT 
                        imeiss.category_name as category_name,
                        imeiss.brand_name as brand_name,
                        imeiss.ram_name as ram_name,
                        imeiss.color_name as color_name,
                        availables.sale_price as sale_price,
                        availables.exchange_price as exchange_price,
                        availables.exchange_name as exchange_name,
                        availables.id as available_id,
                        availables.type as type,
                        imeiss.device_name as device_name,
                        device_images.url as thumb,
                        devices.id as id
                    FROM devices
                    INNER JOIN (
                        SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                            device_availables.type, models.name as exchange_name
                        FROM device_availables
                        LEFT OUTER JOIN models ON device_availables.model_id = models.id
                    ) as availables ON devices.id = availables.device_id
                    INNER JOIN (
                        SELECT imeis.imei, models.name as device_name, models.brand_name, models.category_name,
                            models.ram_name, models.capacity_name, models.color_name
                        FROM imeis
                        INNER JOIN (
                            SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                            FROM models
                            INNER JOIN (
                                SELECT categories.id, categories.name
                                FROM categories
                                `+whereCategory+`
                            )AS categories ON models.category_id = categories.id
                            INNER JOIN (
                                SELECT brands.id, brands.name
                                FROM brands
                                `+whereBrand+`
                            ) AS brands ON models.brand_id = brands.id
                            INNER JOIN (
                                SELECT colors.id, colors.name
                                FROM colors
                                `+whereColor+`
                            ) AS colors ON models.color_id = colors.id
                            INNER JOIN (
                                SELECT capacities.id, capacities.name
                                FROM capacities
                                `+whereCapacity+`
                            ) AS capacities ON models.capacity_id = capacities.id
                            INNER JOIN (
                                SELECT rams.id, rams.name
                                FROM rams
                                `+whereRam+`
                            )rams ON models.ram_id = rams.id
                        ) AS models ON imeis.model_id = models.id
                    ) as imeiss ON imeiss.imei = devices.imei
                    LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                    WHERE devices.user_id <> `+user_id+`
                    `+whereType+`
                    AND availables.type > 0
                    AND devices.id NOT IN(
                        SELECT device_id
                        FROM carts
                        WHERE carts.user_id=`+user_id+`
                    )
                    AND devices.id NOT IN(
                        SELECT device_id
                        FROM transactions
                        WHERE transactions.user_id=`+user_id+`
                    )
                    `
                )
                .then(function(listAll){
                    res.json({status: 200, data: {list: list, total: listAll.length}});
                })
                .catch(function(error){
                    res.status(500).json();
                })
            })
            .catch(function(error){
                res.status(500).json();
            })
        }else{
            res.json({status: 500});
        }
    })
});

router.post('/device/searchByDeviceName', function(req, res){
    var device_name = req.body.device_name;
    var email = req.body.email;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
                `SELECT 
                    imeiss.category_name as category_name,
                    imeiss.brand_name as brand_name,
                    imeiss.ram_name as ram_name,
                    imeiss.color_name as color_name,
                    availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    device_images.url as thumb,
                    devices.id as id,
                    devices.id as value,
                    imeiss.device_name as data
                FROM devices
                INNER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name, models.brand_name, models.category_name,
                        models.ram_name, models.capacity_name, models.color_name
                    FROM imeis
                    INNER JOIN (
                        SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                            colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                        FROM models
                        INNER JOIN (
                            SELECT categories.id, categories.name
                            FROM categories
                        )AS categories ON models.category_id = categories.id
                        INNER JOIN (
                            SELECT brands.id, brands.name
                            FROM brands
                        ) AS brands ON models.brand_id = brands.id
                        INNER JOIN (
                            SELECT colors.id, colors.name
                            FROM colors
                        ) AS colors ON models.color_id = colors.id
                        INNER JOIN (
                            SELECT capacities.id, capacities.name
                            FROM capacities
                        ) AS capacities ON models.capacity_id = capacities.id
                        INNER JOIN (
                            SELECT rams.id, rams.name
                            FROM rams
                        )rams ON models.ram_id = rams.id
                    ) AS models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                WHERE devices.user_id <> `+user_id+`
                AND imeiss.device_name LIKE '%`+device_name+`%'
                AND availables.type > 0
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                )
                AND devices.id NOT IN(
                    SELECT device_id
                    FROM transactions
                    WHERE transactions.user_id=`+user_id+`
                )
                ORDER BY imeiss.device_name ASC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/device/compare', function(req, res){
    var id = req.body.id;
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .asCallback(function(err, rows){
        if(err) return res.status(500).json();
        if(rows.length > 0){
            var user_id = rows[0].id;
            knex
            .raw(
                `
                SELECT availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.id as available_id,
                    availables.type as type,
                    imeiss.device_name as device_name,
                    imeiss.category_name as category_name,
                    imeiss.brand_name as brand_name,
                    imeiss.color_name as color_name,
                    imeiss.capacity_name as capacity_name,
                    imeiss.ram_name as ram_name,
                    devices.condition as device_condition,
                    device_images.url as thumb,
                    devices.id as id,
                    carts.id as cart_id,
                    carts.type as cart_type,
                    wishlist.id as isWishlist
                FROM devices
                INNER JOIN (
                    SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON devices.id = availables.device_id
                INNER JOIN (
                    SELECT imeis.imei, models.name as device_name, models.category_name, models.brand_name,
                        models.color_name, models.capacity_name, models.ram_name
                    FROM imeis
                    INNER JOIN (
                        SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                            colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                        FROM models
                        INNER JOIN categories ON models.category_id = categories.id
                        INNER JOIN brands ON models.brand_id = brands.id
                        INNER JOIN colors ON models.color_id = colors.id
                        INNER JOIN capacities ON models.capacity_id = capacities.id
                        INNER JOIN rams ON models.ram_id = rams.id
                    ) AS models ON imeis.model_id = models.id
                ) as imeiss ON imeiss.imei = devices.imei
                LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                LEFT OUTER JOIN (
                    SELECT carts.device_id, carts.id, carts.type 
                    FROM carts
                    WHERE carts.user_id=`+user_id+`
                ) AS carts ON carts.device_id = devices.id
                LEFT OUTER JOIN (
                    SELECT wishlist.id, wishlist.device_id
                    FROM wishlist
                    WHERE wishlist.device_id = `+id+` AND wishlist.user_id = `+user_id+`
                ) AS wishlist ON wishlist.device_id = devices.id
                WHERE devices.id=`+id
            )
            .asCallback(function(err1, rows1){
                if(err1) return res.status(500).json();
                if(rows1.length > 0){
                    var mainProduct = rows1[0];
                    knex
                    .raw(
                        `
                        SELECT availables.sale_price as sale_price,
                            availables.exchange_price as exchange_price,
                            availables.exchange_name as exchange_name,
                            availables.id as available_id,
                            availables.type as type,
                            imeiss.device_name as device_name,
                            imeiss.category_name as category_name,
                            imeiss.brand_name as brand_name,
                            imeiss.color_name as color_name,
                            imeiss.capacity_name as capacity_name,
                            imeiss.ram_name as ram_name,
                            devices.condition as device_condition,
                            device_images.url as thumb,
                            devices.id as id,
                            carts.id as cart_id,
                            carts.type as cart_type,
                            wishlist.id as isWishlist
                        FROM devices
                        INNER JOIN (
                            SELECT device_availables.id, device_availables.device_id, device_availables.sale_price, device_availables.exchange_price,
                                device_availables.type, models.name as exchange_name
                            FROM device_availables
                            LEFT OUTER JOIN models ON device_availables.model_id = models.id
                        ) as availables ON devices.id = availables.device_id
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.category_name, models.brand_name,
                                models.color_name, models.capacity_name, models.ram_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) as imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                        LEFT OUTER JOIN (
                            SELECT carts.device_id, carts.id, carts.type 
                            FROM carts
                            WHERE carts.user_id=`+user_id+`
                        ) AS carts ON carts.device_id = devices.id
                        LEFT OUTER JOIN (
                            SELECT wishlist.id, wishlist.device_id
                            FROM wishlist
                            WHERE wishlist.device_id = `+id+` AND wishlist.user_id = `+user_id+`
                        ) AS wishlist ON wishlist.device_id = devices.id
                        WHERE devices.id<>`+mainProduct.id+`
                        AND devices.user_id <> `+user_id+`
                        ORDER BY imeiss.category_name ASC`
                    )
                    .asCallback(function(err2, rows2){
                        if(err2) {
                            return res.status(500).json();
                        }
                        res.json({status: 200, data: {main: mainProduct, listCompare: rows2}});
                    })
                }
            })
        }
    })
});
router.get('/slide/list', function(req, res){
    knex('slides')
    .select('id', 'name','image')
    .orderBy('id','desc')
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
//admin
router.post('/category/list', function(req, res){
    var name = req.body.search.name;
    knex.raw(
        `SELECT
            id, name
        FROM categories
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/category/add', function(req, res){
    var name = req.body.name;

    knex('categories')
    .insert({
        name: name,
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/category/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;

    knex('categories')
    .update({
        name: name
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/category/delete', function(req, res){
    var id = req.body.id;
    knex('categories')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.get('/category/detail/:id', function(req, res){
    var id = req.params.id;
    knex('categories')
    .select('id', 'name')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/brand/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `SELECT
            id, name
        FROM brands
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/brand/add', function(req, res){
    var name = req.body.name;

    knex('brands')
    .insert({
        name: name,
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/brand/delete', function(req, res){
    var id = req.body.id;
    knex('brands')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.get('/brand/detail/:id', function(req, res){
    var id = req.params.id;
    knex('brands')
    .select('id', 'name')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/brand/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;

    knex('brands')
    .update({
        name: name
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/brand/image/:id', function(req, res){
    var id = req.params.id;
    var query = knex('brands')
                .select('id', 'image')
                .where('id', id)
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/brand/imageUpload', function(req, res, next){
    var storage = multer.diskStorage({
        destination: function(req, file, cb){
            var dest = path.resolve(__dirname, '..', 'uploads', 'brands');
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    
    var upload = multer({ storage: storage });
    upload.single('photo')(req, res, function(err){
        if (err instanceof multer.MulterError) {
            res.json({status: 400});
        } else if (err) {
            res.json({status: 400});
        }
        var id = req.body.id;
        var filename = req.file.filename;

        knex('brands')
        .update({
            image: filename,
        })
        .where({'id':id})
        .then(function(updated){
            res.json({status: 200});
        })
        .catch(function(error){
            res.status(500).json();
        })
    })
});
router.delete('/brand/delete/image', function(req, res){
    var id = req.body.id;
    var query = knex('brands')
                .select('id', 'image')
                .where('id', id)
    query.then(function(rows){
        if(rows.length > 0){
            knex('brands')
            .where('id', id)
            .update('image', '')
            .then(function(deleted){
                var file = path.resolve(__dirname, '..', 'uploads', 'brands', rows[0].image);
                fs.unlink(file, (err) => {
                    if (err) {
                        res.status(500).json();
                        return;
                    }
                    
                })
                res.json({status: 200, data: deleted});
            })
            .catch(function(error){
                res.status(500).json();
            });
        }else{
            res.json({status: 500});
        }
    })
});
router.get('/model/list', function(req, res){
    knex.raw(
        `select
            models.id as id,
            models.name as model_name,
            categories.name as category_name,
            categories.id as category_id,
            brands.name as brand_name,
            brands.id as brand_id,
            colors.name as color_name,
            colors.id as color_id,
            capacities.name as capacity_name,
            rams.name as ram_name,
            rams.id as ram_id,
            models.price as price,
            models.image as url
        from models	
            inner join categories on categories.id = models.category_id
            inner join brands on brands.id = models.brand_id
            inner join colors on colors.id = models.color_id
            inner join capacities on capacities.id = models.capacity_id
            inner join rams on rams.id = models.ram_id
        order by models.id DESC
        `
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/model/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `select
            models.id as id,
            models.name as model_name,
            categories.name as category_name,
            categories.id as category_id,
            brands.name as brand_name,
            brands.id as brand_id,
            colors.name as color_name,
            colors.id as color_id,
            capacities.name as capacity_name,
            rams.name as ram_name,
            rams.id as ram_id,
            models.price as price,
            models.image as url
        from models	
            inner join categories on categories.id = models.category_id
            inner join brands on brands.id = models.brand_id
            inner join colors on colors.id = models.color_id
            inner join capacities on capacities.id = models.capacity_id
            inner join rams on rams.id = models.ram_id
        WHERE models.name LIKE '%`+name+`%'
        order by models.id DESC
        `
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/model/add', function(req, res){
    var name = req.body.name;
    var category_id = req.body.category_id;
    var brand_id = req.body.brand_id;
    var color_id = req.body.color_id;
    var capacity_id = req.body.capacity_id;
    var ram_id = req.body.ram_id;
    var price = req.body.price;

    knex('models')
    .insert({
        name: name,
        category_id: category_id,
        brand_id: brand_id,
        color_id: color_id,
        capacity_id: capacity_id,
        ram_id: ram_id,
        price: price,
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/model/detail/:id', function(req, res){
    var id = req.params.id;
    knex.raw(
        `select
            models.id as id,
            models.name as model_name,
            categories.name as category_name,
            categories.id as category_id,
            brands.name as brand_name,
            brands.id as brand_id,
            colors.name as color_name,
            colors.id as color_id,
            capacities.name as capacity_name,
            rams.name as ram_name,
            rams.id as ram_id,
            models.price as price
        from models	
            inner join categories on categories.id = models.category_id
            inner join brands on brands.id = models.brand_id
            inner join colors on colors.id = models.color_id
            inner join capacities on capacities.id = models.capacity_id
            inner join rams on rams.id = models.ram_id
        where models.id=`+id+`
        `
    )
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/model/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;
    var category_id = req.body.category_id;
    var brand_id = req.body.brand_id;
    var color_id = req.body.color_id;
    var capacity_id = req.body.capacity_id;
    var ram_id = req.body.ram_id;
    var price = req.body.price;

    knex('models')
    .update({
        name: name,
        category_id: category_id,
        brand_id: brand_id,
        color_id: color_id,
        capacity_id: capacity_id,
        ram_id: ram_id,
        price: price,
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/model/delete', function(req, res){
    var id = req.body.id;
    knex('models')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.get('/model/image/:id', function(req, res){
    var id = req.params.id;
    var query = knex('models')
                .select('id', 'image')
                .where('id', id)
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/model/imageUpload', function(req, res, next){
    var storage = multer.diskStorage({
        destination: function(req, file, cb){
            var dest = path.resolve(__dirname, '..', 'uploads', 'models');
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    
    var upload = multer({ storage: storage });
    upload.single('photo')(req, res, function(err){
        if (err instanceof multer.MulterError) {
            res.json({status: 400});
        } else if (err) {
            res.json({status: 400});
        }
        var id = req.body.id;
        var filename = req.file.filename;

        knex('models')
        .update({
            image: filename,
        })
        .where({'id':id})
        .then(function(updated){
            res.json({status: 200});
        })
        .catch(function(error){
            res.status(500).json();
        })
    })
});
router.delete('/model/delete/image', function(req, res){
    var id = req.body.id;
    var query = knex('models')
                .select('id', 'image')
                .where('id', id)
    query.then(function(rows){
        if(rows.length > 0){
            knex('models')
            .where('id', id)
            .update('image', '')
            .then(function(deleted){
                var file = path.resolve(__dirname, '..', 'uploads', 'models', rows[0].image);
                fs.unlink(file, (err) => {
                    if (err) {
                        res.status(500).json();
                        return;
                    }
                    
                })
                res.json({status: 200, data: deleted});
            })
            .catch(function(error){
                res.status(500).json();
            });
        }else{
            res.json({status: 500});
        }
    })
});
router.post('/imei/list', function(req, res){
    var name = req.body.search.name;
    knex.raw(
        `select imeis.id, imeis.imei , imeis.model_id, models.name as model_name
        from imeis
        inner join models on models.id = imeis.model_id    
        WHERE imeis.imei LIKE '%`+name+`%'    
        order by imeis.id DESC
        `
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/imei/delete', function(req, res){
    var id = req.body.id;
    knex('imeis')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/imei/add', function(req, res){
    var imei = req.body.imei;
    var model_id = req.body.model_id;
    
    knex('imeis')
    .insert({
        imei: imei,
        model_id:model_id
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/imei/edit', function(req, res){
    var id = req.body.id;
    var imei = req.body.imei;
    var model_id = req.body.model_id;
    knex('imeis')
    .update({
        imei: imei,
        model_id:model_id
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/imei/detail/:id', function(req, res){
    var id = req.params.id;
    knex.raw(
        `select imeis.id, imeis.imei , imeis.model_id, models.name as model_name
        from imeis
        inner join models on models.id = imeis.model_id        
        where imeis.id = `+id+`
        `
    )
    .then(function(list){
        res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/color/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `SELECT
            id, name, code
        FROM colors
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/color/add', function(req, res){
    var name = req.body.name;
    var code = req.body.code;
   
    knex('colors')
    .insert({
        name: name,
        code: code
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/color/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;
    var code = req.body.code;

    knex('colors')
    .update({
        name: name,
        code: code
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/color/detail/:id', function(req, res){
    var id = req.params.id;
    knex('colors')
    .select('id', 'name','code')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/color/delete', function(req, res){
    var id = req.body.id;
    knex('colors')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/capacity/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `SELECT
            id, name
        FROM capacities
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/capacity/add', function(req, res){
    var name = req.body.name;
   
    knex('capacities')
    .insert({
        name: name
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/capacity/delete', function(req, res){
    var id = req.body.id;
    knex('capacities')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/capacity/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;

    knex('capacities')
    .update({
        name: name
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/capacity/detail/:id', function(req, res){
    var id = req.params.id;
    knex('capacities')
    .select('id', 'name')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/ram/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `SELECT
            id, name
        FROM rams
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/ram/add', function(req, res){
    var name = req.body.name;
   
    knex('rams')
    .insert({
        name: name
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/ram/delete', function(req, res){
    var id = req.body.id;
    knex('rams')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/ram/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;

    knex('rams')
    .update({
        name: name
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/ram/detail/:id', function(req, res){
    var id = req.params.id;
    knex('rams')
    .select('id', 'name')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/slide/list', function(req, res){
    var name = req.body.search.name;    
    knex.raw(
        `SELECT
            id, name, image
        FROM slides
        WHERE name LIKE '%`+name+`%'
        ORDER BY name DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/slide/add', function(req, res){
    var name = req.body.name;

    knex('slides')
    .insert({
        name: name,
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/slide/delete', function(req, res){
    var id = req.body.id;
    knex('slides')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/slide/edit', function(req, res){
    var id = req.body.id;
    var name = req.body.name;

    knex('slides')
    .update({
        name: name
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/slide/detail/:id', function(req, res){
    var id = req.params.id;
    knex('slides')
    .select('id', 'name')
    .where({
        id: id
    })
    .then(function(list){
        if(list.length > 0)
            res.json({status: 200, data: list[0]});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/slide/image/:id', function(req, res){
    var id = req.params.id;
    var query = knex('slides')
                .select('id', 'image')
                .where('id', id)
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/slide/imageUpload', function(req, res, next){
    var storage = multer.diskStorage({
        destination: function(req, file, cb){
            var dest = path.resolve(__dirname, '..', 'uploads', 'slides');
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    
    var upload = multer({ storage: storage });
    upload.single('photo')(req, res, function(err){
        if (err instanceof multer.MulterError) {
            res.json({status: 400});
        } else if (err) {
            console.log("dsa", err)
            res.json({status: 400});
        }
        var id = req.body.id;
        var filename = req.file.filename;

        knex('slides')
        .update({
            image: filename,
        })
        .where({'id':id})
        .then(function(updated){
            res.json({status: 200});
        })
        .catch(function(error){
            res.status(500).json();
        })
    })
});
router.delete('/slide/delete/image', function(req, res){
    var id = req.body.id;
    var query = knex('slides')
                .select('id', 'image')
                .where('id', id)
    query.then(function(rows){
        if(rows.length > 0){
            knex('slides')
            .where('id', id)
            .update('image', '')
            .then(function(deleted){
                var file = path.resolve(__dirname, '..', 'uploads', 'slides', rows[0].image);
                fs.unlink(file, (err) => {
                    if (err) {
                        res.status(500).json();
                        return;
                    }
                    
                })
                res.json({status: 200, data: deleted});
            })
            .catch(function(error){
                res.status(500).json();
            });
        }else{
            res.json({status: 500});
        }
    })
});
module.exports = router;